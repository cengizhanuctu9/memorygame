﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gamecontrol : MonoBehaviour
{
    int checkcount;
    GameObject beforbutton;
    GameObject nowbutton;
    public Sprite defaultsprite;
    public GameObject[] buttons;
    public GameObject gameoverpanel;
    public GameObject winpanel;
    public GameObject resumepausepanel;
    public Text timetext;
    public float totaltime = 68;
    public float minute;
    public float second;
    public GameObject grid;
    public GameObject objectpool;
    bool maketile = true;
    int truemach;
    float objetpoolchilcount;


    private void Start()
    {
        checkcount = 0;
        objetpoolchilcount = objectpool.transform.childCount / 2;
        StartCoroutine(randomtile());
    }
    public void getbutton(GameObject button)//sahnedeki butonların onclik fonksyonunda butonu obje olarak alıyoruz her buton değer olarak kendisini gönderiyor
    {
        nowbutton = button;

        //getcompanet in çalışabilmesi için UnityEngine.UI gerekliydi 

        nowbutton.GetComponent<Image>().sprite = nowbutton.GetComponentInChildren<SpriteRenderer>().sprite;
        nowbutton.GetComponent<Image>().raycastTarget = false;//aynı butona bir kere tıklana bilir yapıyoruz
    }
    public void oncilick(int value)
    {
        check(value, nowbutton);

    }
    private void buttonstatus(bool status)
    {
       
        foreach (var item in buttons)
        {
            if (item != null)
            {
                item.GetComponent<Image>().raycastTarget = status;//burada item yerine now buron yazdığım için oyunda hata oluştu
            }

        }

    }
    void Update()
    {
        if (totaltime >1) 
        {
            totaltime -= Time.deltaTime;
            minute = Mathf.FloorToInt(totaltime / 60);//60 a bölüyoruz ve sonucu en yakın int e yuvarlıor mesela 100/60=1.66 bunu 1 e yuvarlıyor
            second = Mathf.FloorToInt(totaltime % 60);
            timetext.text = string.Format("{0:00} : {1:00}", minute, second);
        }
        else
        {
            gameover();
        }
        if (truemach == objetpoolchilcount)
        {
            win();
        }
    }

    void check(int value, GameObject objj)
    {

        if (checkcount == 0)// eğer önceden value gönderilmediyse value 0 dır 
        {
            checkcount = value;// bir önceki degeri bir dahaki tıklama için checkcount ile saklıyoruz yeni gelecek degerimiz value olarak gelecek
            beforbutton = objj;
        }
        else
        {
            StartCoroutine(timeing(value));//aslında iki tıklamada checkcount u kontrol edebiliyoruz ilk baş deger atıyoruz sonra değeri var ise yeni gelen değer ile karşılastırıyoruz
            
        }
    }
    IEnumerator randomtile()
    {
        yield return new WaitForSeconds(.1f);
       
        while (maketile)
        {
            int rnd = Random.Range(0, objectpool.transform.childCount - 1);
            if (objectpool.transform.GetChild(rnd) != null)
            {
                objectpool.transform.GetChild(rnd).transform.SetParent(grid.transform);
            }
            if (objectpool.transform.childCount == 0)
            {
                maketile = false;
                Destroy(objectpool);
            }
        }
    }
    IEnumerator timeing(int value)//zamanlayıcı
    {
        buttonstatus(false);
        yield return new WaitForSeconds(.4f);
        if (checkcount == value)// value her tıklanmada bir gönderiliyor önce value gönderiliyor ve bir önceki
        {
           
            beforbutton.GetComponent<Image>().enabled = false;
            beforbutton.GetComponent<Button>().enabled = false;
            nowbutton.GetComponent<Image>().enabled = false;
            nowbutton.GetComponent<Button>().enabled = false;
            buttonstatus(true);
            checkcount = 0;
            beforbutton = null;
            truemach++;
           
        }
        else
        {
            buttonstatus(true);
            beforbutton.GetComponent<Image>().sprite = defaultsprite;
            nowbutton.GetComponent<Image>().sprite = defaultsprite;
            checkcount = 0; 
            beforbutton = null;
           
        }
    }
   public void playagain() 
    {
       
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
   public void mainmenu() 
    {
        SceneManager.LoadScene(0);
    }
    public void pausegame()
    {
        Time.timeScale = 0;
    }
    public void resumegame()
    {
        Time.timeScale = 1;
        resumepausepanel.SetActive(false);
    }
    void gameover()
    {
        gameoverpanel.SetActive(true);

    }
    void win()
    {
        winpanel.SetActive(true);

    }
    public void resumepause()
    {
        resumepausepanel.SetActive(true);

    }
}
