﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class gamemanager : MonoBehaviour
{
    public GameObject quitgamepanel;
    public GameObject startquitbutton;
    private void Start()
    {
        Time.timeScale = 1;
    }
    public void QUITGAME()
    {
        startquitbutton.SetActive(false);
        quitgamepanel.SetActive(true);
    }
    public void YES()
    {
        Application.Quit();
    }
    public void NO()
    {
        quitgamepanel.SetActive(false);
        startquitbutton.SetActive(true);
    }
    public void Startgame()
    {
        SceneManager.LoadScene(1);
    }
}
