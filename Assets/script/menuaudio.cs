﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menuaudio : MonoBehaviour
{
    private static GameObject instance;// static objeler bir kere tanımlanır ve deiştirilmez 
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);// sahne geçislerinde obje taşımaya yarıyor
        if (instance == null)
        {
            instance = gameObject;
        }
        else
        {
            Destroy(gameObject);
        }

    }
}
